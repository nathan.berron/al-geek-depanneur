package fr.afcepf.al34.depanneur.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.depanneur.entity.Depanneur;


/**
 * Dao used for CRU operations on Depanneur
 */
public interface DepanneurDao extends CrudRepository<Depanneur, Long> {

	List<Depanneur> findAll();
	
	List<Depanneur> findByVille(String ville);
	
//	Optional<Depanneur> findById(Long id);
	

}
