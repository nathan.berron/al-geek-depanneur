package fr.afcepf.al34.depanneur.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * Depanneur entity
 */
@Entity
@Getter @Setter @NoArgsConstructor
public class Depanneur implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "societe")
	private String nomSociete;
	
	private String adresse;

	private String ville; 
	
	private String telephone;
	
	@Column(name="latitude")
	private double lat;
	
	@Column(name="longitude")
	private double lon;
	
	private String email;

	public Depanneur(String nomSociete, String adresse, String ville, String telephone, double lat, double lon,
			String email) {
		super();
		this.nomSociete = nomSociete;
		this.adresse = adresse;
		this.ville = ville;
		this.telephone = telephone;
		this.lat = lat;
		this.lon = lon;
		this.email = email;
	}
	
}
