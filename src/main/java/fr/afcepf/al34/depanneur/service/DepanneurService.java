package fr.afcepf.al34.depanneur.service;

import java.util.List;

import fr.afcepf.al34.depanneur.entity.Depanneur;


/**
 * Service used for CRU operations on Depanneur
 */
public interface DepanneurService {

	List<Depanneur> getAllDepanneurs();
	List<Depanneur> getDepanneurByVille(String ville);
	Depanneur addNewDepanneur(Depanneur depanneur);
	Depanneur modifier(Depanneur depanneur);
//	Depanneur rechercherById(Long id);

}
