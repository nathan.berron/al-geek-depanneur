package fr.afcepf.al34.depanneur.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afcepf.al34.depanneur.dao.DepanneurDao;
import fr.afcepf.al34.depanneur.entity.Depanneur;


/**
 * Service used for CRU operations on Depanneur
 */
@Service
@Transactional
public class DepanneurServiceImpl implements DepanneurService {

	
	/**
	 * dao of Depanneur used by DepanneurService to do basic CRUD operations
	 */
	@Autowired
	private DepanneurDao dao;
	
	
	/**
	 *Retrieves the list of all IT repairer
	 */
	@Override
	public List<Depanneur> getAllDepanneurs() {
		return dao.findAll();
	}

	
	/**
	 *Retrieves the list of all IT repairers by city
	 *
	 *@param ville The city where you want to find the available IT repairers
	 */
	@Override
	public List<Depanneur> getDepanneurByVille(String ville) {
		return dao.findByVille(ville);
	}

	
	/**
	 *Add a new IT repairer
	 *
	 *@param depanneur The repairer you wish to add
	 */
	@Override
	public Depanneur addNewDepanneur(Depanneur depanneur) {
		return dao.save(depanneur);
	}

	
	/**
	 *Update the informations of an IT repairer
	 *
	 *@param depanneur The repairer you wish to add
	 */
	@Override
	public Depanneur modifier(Depanneur depanneur) {
		return dao.save(depanneur);
	}


//	@Override
//	public Depanneur rechercherById(Long id) {
//		return dao.findById(id).orElse(null);
//	}
	
	
	
}
